﻿using System.ComponentModel.DataAnnotations;

namespace TP.Front.Application.Models
{
    public class CreatePQRModel
    {
        [Required(ErrorMessage = "Es requerido")]
        [DataType(DataType.Text)]
        public string Comment { get; set; }
        [Required(ErrorMessage = "Es requerido")]
        public long IdCustomer { get; set; }
        [Required(ErrorMessage = "Es requerido")]
        public string PQRTypeCode { get; set; }
    }
}
