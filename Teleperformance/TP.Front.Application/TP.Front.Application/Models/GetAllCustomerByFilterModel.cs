﻿using System.Collections.Generic;
using TP.Transverse.Models.Response.Customer;

namespace TP.Front.Application.Models
{
    public class GetAllCustomerByFilterModel
    {
        public List<CustomerResponse> CustomerssModel { get; set; }
        public CustomersAverageResponse CustomersAverage { get; set; }
    }
}
