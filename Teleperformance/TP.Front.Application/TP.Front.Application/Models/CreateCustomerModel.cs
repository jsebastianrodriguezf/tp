﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TP.Front.Application.Models
{
    public class CreateCustomerModel
    {
        [Required(ErrorMessage = "Es requerido")]
        [DataType(DataType.Text)]
        public string FullName { get; set; }
        [Required(ErrorMessage = "Es requerido")]
        public string DocumentTypeCode { get; set; }
        [Required(ErrorMessage = "Es requerido")]
        [DataType(DataType.Text)]
        public string DocumentNumber { get; set; }
        public string PhoneNumber { get; set; } = string.Empty;
        [Required(ErrorMessage = "Es requerido")]
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }
        [Required(ErrorMessage = "Es requerido")]
        public int IdCity { get; set; }
    }
}
