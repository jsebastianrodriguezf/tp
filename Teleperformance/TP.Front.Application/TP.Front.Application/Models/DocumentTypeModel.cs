﻿namespace TP.Front.Application.Models
{
    public class DocumentTypeModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
