﻿using System.ComponentModel.DataAnnotations;

namespace TP.Front.Application.Models
{
    public class UpdateCustomerModel
    {
        [Required(ErrorMessage = "Es requerido")]
        public long IdCustomer { get; set; }
        [Required(ErrorMessage = "Es requerido")]
        [DataType(DataType.Text)]
        public string PhoneNumber { get; set; } = string.Empty;
        [Required(ErrorMessage = "Es requerido")]
        public int IdCity { get; set; }
    }
}
