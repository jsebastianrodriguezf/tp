﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TP.Front.Application.Models
{
    public class PQRByFilterModel
    {
        [Required(ErrorMessage = "Es requerido")]
        [DataType(DataType.Date)]
        public DateTime From { get; set; }
        [Required(ErrorMessage = "Es requerido")]
        [DataType(DataType.Date)]
        public DateTime Until { get; set; }
        [Required(ErrorMessage = "Es requerido")]
        public int? IdCity { get; set; }
        public string DocumentNumber { get; set; } = string.Empty;
    }
}
