﻿using System.ComponentModel.DataAnnotations;

namespace TP.Front.Application.Models
{
    public class CustomerByDocumentModel
    {
        [Required(ErrorMessage = "Es requerido")]
        public string DocumentTypeCode { get; set; }
        [Required(ErrorMessage = "Es requerido")]
        [DataType(DataType.Text)]
        public string DocumentNumber { get; set; }
    }
}
