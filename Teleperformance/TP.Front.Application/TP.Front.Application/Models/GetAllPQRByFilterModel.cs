﻿using System.Collections.Generic;
using TP.Transverse.Models.Response.Customer;

namespace TP.Front.Application.Models
{
    public class GetAllPQRByFilterModel
    {
        public List<PQRModel> PQRsModel { get; set; }
        public CustomersAverageResponse CustomersAverage { get; set; }
    }
}
