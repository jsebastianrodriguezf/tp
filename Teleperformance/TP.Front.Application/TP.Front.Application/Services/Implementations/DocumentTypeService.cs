﻿using Newtonsoft.Json;
using System.Collections.Generic;
using AutoMapper;
using System.Threading.Tasks;
using TP.Front.Application.Models;
using TP.Front.Application.Repository.Interfaces;
using TP.Front.Application.Services.Interfaces;
using TP.Transverse.Models.Response;
using TP.Transverse.Models.Response.DocumentType;
using static TP.Transverse.Models.Response.ResponseCode;

namespace TP.Front.Application.Services.Implementations
{
    public class DocumentTypeService : IDocumentTypeService
    {
        private readonly IDocumentTypeRepository _documentTypeRepository;
        private readonly IMapper _mapper; 

        public DocumentTypeService(IDocumentTypeRepository documentTypeRepository, IMapper mapper)
        {
            _documentTypeRepository = documentTypeRepository;
            _mapper = mapper;
        }

        public async Task<List<DocumentTypeModel>> GetAllDocumentTypes()
        {
            BaseApiResponseWithData response = await _documentTypeRepository.GetAllDocumentTypes();
            if (response.StatusCode != (int)StatusCodeEnum.OK)
                return new List<DocumentTypeModel>();

            List<DocumentTypeResponse> documentTypes = JsonConvert.DeserializeObject<List<DocumentTypeResponse>>(response.Data.ToString());
            List<DocumentTypeModel> documentTypeModels = _mapper.Map<List<DocumentTypeResponse>, List<DocumentTypeModel>>(documentTypes);

            return documentTypeModels;
        }
    }
}
