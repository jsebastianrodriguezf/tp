﻿using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TP.Front.Application.Models;
using TP.Front.Application.Repository.Interfaces;
using TP.Front.Application.Services.Interfaces;
using TP.Transverse.Models.Request.Customer;
using TP.Transverse.Models.Response;
using TP.Transverse.Models.Response.Customer;
using static TP.Transverse.Models.Response.ResponseCode;

namespace TP.Front.Application.Services.Implementations
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;

        public CustomerService(ICustomerRepository customerRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<CustomerResponse> CreateCustomer(CreateCustomerModel createCustomerModel)
        {
            CreateCustomerRequest customerRequest = _mapper.Map<CreateCustomerModel, CreateCustomerRequest>(createCustomerModel);
            BaseApiResponseWithData response = await _customerRepository.CreateCustomer(customerRequest);
            if (response.StatusCode != (int)StatusCodeEnum.OK)
                return null;

            return JsonConvert.DeserializeObject<CustomerResponse>(response.Data.ToString());
        }

        public async Task<CustomerResponse> GetCustomer(long idCustomer)
        {
            BaseApiResponseWithData response = await _customerRepository.GetCustomer(idCustomer);
            if (response.StatusCode != (int)StatusCodeEnum.OK)
                return null;

            return JsonConvert.DeserializeObject<CustomerResponse>(response.Data.ToString());
        }

        public async Task<CustomerResponse> GetCustomerByDocument(CustomerByDocumentModel customerByDocumentModel)
        {
            GetCustomerByDocumentRequest customerDocument = _mapper.Map<CustomerByDocumentModel, GetCustomerByDocumentRequest>(customerByDocumentModel);
            BaseApiResponseWithData response = await _customerRepository.GetCustomerByDocument(customerDocument);
            if (response.StatusCode != (int)StatusCodeEnum.OK)
                return null;

            return JsonConvert.DeserializeObject<CustomerResponse>(response.Data.ToString());
        }

        public async Task<CustomerResponse> UpdateCustomer(UpdateCustomerModel updateCustomerModel)
        {
            UpdateCustomerRequest updateCustomer = _mapper.Map<UpdateCustomerModel, UpdateCustomerRequest>(updateCustomerModel);
            BaseApiResponseWithData response = await _customerRepository.UpdateCustomer(updateCustomer);
            if (response.StatusCode != (int)StatusCodeEnum.OK)
                return null;

            return JsonConvert.DeserializeObject<CustomerResponse>(response.Data.ToString());
        }

        public async Task<CustomersAverageResponse> GetCustomerAverage()
        {
            BaseApiResponseWithData response = await _customerRepository.GetCustomerAverage();
            if (response.StatusCode != (int)StatusCodeEnum.OK)
                return new CustomersAverageResponse();

            return JsonConvert.DeserializeObject<CustomersAverageResponse>(response.Data.ToString());
        }

        public async Task<GetAllCustomerByFilterModel> GetCustomerByFilter(CustomerByFilterModel model)
        {
            if (DateTime.MinValue.Equals(model.From))
                model.From = new DateTime(1980, 1, 1);
            if (DateTime.MinValue.Equals(model.Until))
                model.Until = DateTime.MaxValue;

            CustomerByFilterRequest customerFilter = _mapper.Map<CustomerByFilterModel, CustomerByFilterRequest>(model);

            Task<BaseApiResponseWithData> customerFilterTask = _customerRepository.GetCustomerByFilter(customerFilter);
            Task<CustomersAverageResponse> customerAverageTask = GetCustomerAverage();

            await Task.WhenAll(customerFilterTask, customerAverageTask);

            GetAllCustomerByFilterModel getAllCustomer = new GetAllCustomerByFilterModel()
            {
                CustomersAverage = customerAverageTask.Result,
                CustomerssModel = new List<CustomerResponse>()
            };

            BaseApiResponseWithData response = customerFilterTask.Result;
            if (response.StatusCode == (int)StatusCodeEnum.OK)
                getAllCustomer.CustomerssModel = JsonConvert.DeserializeObject<List<CustomerResponse>>(response.Data.ToString());

            return getAllCustomer;
        }
    }
}
