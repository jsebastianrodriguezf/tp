﻿using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TP.Front.Application.Models;
using TP.Front.Application.Repository.Interfaces;
using TP.Front.Application.Services.Interfaces;
using TP.Transverse.Models.Request.PQR;
using TP.Transverse.Models.Response;
using TP.Transverse.Models.Response.Customer;
using TP.Transverse.Models.Response.PQR;
using static TP.Transverse.Models.Response.ResponseCode;

namespace TP.Front.Application.Services.Implementations
{
    public class PQRService : IPQRService
    {
        private readonly ICustomerService _customerService;
        private readonly IPQRRepository _pQRRepository;
        private readonly IMapper _mapper;

        public PQRService(IPQRRepository pQRRepository, ICustomerService customerService, IMapper mapper)
        {
            _pQRRepository = pQRRepository;
            _customerService = customerService;
            _mapper = mapper;
        }

        public async Task<List<PQRModel>> GetAllPQRs()
        {
            BaseApiResponseWithData baseApiResponse = await _pQRRepository.GetAllPQRs();
            if (baseApiResponse.StatusCode != (int)StatusCodeEnum.OK)
                return new List<PQRModel>();

            List<PQRResponse> pQRResponses = JsonConvert.DeserializeObject<List<PQRResponse>>(baseApiResponse.Data.ToString());
            List<PQRModel> pQRsModel = _mapper.Map<List<PQRResponse>, List<PQRModel>>(pQRResponses);

            return pQRsModel;
        }

        public async Task<GetAllPQRByFilterModel> GetPQRByFilter(PQRByFilterModel model)
        {
            if (DateTime.MinValue.Equals(model.From))
                model.From = new DateTime(1980, 1, 1);
            if (DateTime.MinValue.Equals(model.Until))
                model.Until = DateTime.MaxValue;

            GetPQRByFilterRequest getPQRByFilter = _mapper.Map<PQRByFilterModel, GetPQRByFilterRequest>(model);

            Task<BaseApiResponseWithData> pqrFilterTask = _pQRRepository.GetPQRByFilter(getPQRByFilter);
            Task<CustomersAverageResponse> customerAverageTask = _customerService.GetCustomerAverage();

            await Task.WhenAll(pqrFilterTask, customerAverageTask);

            GetAllPQRByFilterModel getAllPQR = new GetAllPQRByFilterModel()
            {
                CustomersAverage = customerAverageTask.Result,
                PQRsModel = new List<PQRModel>()
            };

            BaseApiResponseWithData response = pqrFilterTask.Result;
            if (response.StatusCode == (int)StatusCodeEnum.OK)
            {
                List<PQRResponse> pQRResponses = JsonConvert.DeserializeObject<List<PQRResponse>>(response.Data.ToString());
                getAllPQR.PQRsModel = _mapper.Map<List<PQRResponse>, List<PQRModel>>(pQRResponses);
            }

            return getAllPQR;
        }

        public async Task<PQRModel> GetPQRByIdentifier(Guid identifier)
        {
            BaseApiResponseWithData baseApiResponse = await _pQRRepository.GetPQRByIdentifier(identifier);
            if (baseApiResponse.StatusCode != (int)StatusCodeEnum.OK)
                return null;

            PQRResponse pQRResponse = JsonConvert.DeserializeObject<PQRResponse>(baseApiResponse.Data.ToString());
            PQRModel pQRsModel = _mapper.Map<PQRResponse, PQRModel>(pQRResponse);

            return pQRsModel;
        }

        public async Task<List<PQRTypeResponse>> GetAllPQRTypes()
        {
            BaseApiResponseWithData baseApiResponse = await _pQRRepository.GetAllPQRTypes();
            if (baseApiResponse.StatusCode != (int)StatusCodeEnum.OK)
                return new List<PQRTypeResponse>();

            return JsonConvert.DeserializeObject<List<PQRTypeResponse>>(baseApiResponse.Data.ToString());
        }

        public async Task<PQRResponse> CreatePQR(CreatePQRModel request)
        {
            CreatePQRRequest createPQR = _mapper.Map<CreatePQRModel, CreatePQRRequest>(request);
            BaseApiResponseWithData response = await _pQRRepository.CreatePQR(createPQR);
            if (response.StatusCode != (int)StatusCodeEnum.OK)
                return null;

            return JsonConvert.DeserializeObject<PQRResponse>(response.Data.ToString());
        }
    }
}
