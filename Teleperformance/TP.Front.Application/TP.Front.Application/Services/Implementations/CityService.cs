﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TP.Front.Application.Repository.Interfaces;
using TP.Front.Application.Services.Interfaces;
using TP.Transverse.Models.Response;
using TP.Transverse.Models.Response.City;
using static TP.Transverse.Models.Response.ResponseCode;

namespace TP.Front.Application.Services.Implementations
{
    public class CityService : ICityService
    {
        private readonly ICityRepository cityRepository;

        public CityService(ICityRepository cityRepository)
        {
            this.cityRepository = cityRepository;
        }

        public async Task<List<CityResponse>> GetAllCities()
        {
            BaseApiResponseWithData response = await cityRepository.GetAllCities();
            if (response.StatusCode != (int)StatusCodeEnum.OK)
                return new List<CityResponse>();

            return JsonConvert.DeserializeObject<List<CityResponse>>(response.Data.ToString());
        }
    }
}
