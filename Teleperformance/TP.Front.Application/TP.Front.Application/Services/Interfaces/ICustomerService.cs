﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TP.Front.Application.Models;
using TP.Transverse.Models.Response.Customer;

namespace TP.Front.Application.Services.Interfaces
{
    public interface ICustomerService
    {
        Task<CustomerResponse> GetCustomer(long idCustomer);
        Task<CustomerResponse> GetCustomerByDocument(CustomerByDocumentModel customerByDocumentModel);
        Task<CustomerResponse> CreateCustomer(CreateCustomerModel createCustomerModel);
        Task<CustomerResponse> UpdateCustomer(UpdateCustomerModel updateCustomerModel);
        Task<CustomersAverageResponse> GetCustomerAverage();
        Task<GetAllCustomerByFilterModel> GetCustomerByFilter(CustomerByFilterModel customerByFilterModel);
    }
}
