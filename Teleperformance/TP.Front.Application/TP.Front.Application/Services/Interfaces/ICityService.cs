﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TP.Transverse.Models.Response.City;

namespace TP.Front.Application.Services.Interfaces
{
    public interface ICityService
    {
        Task<List<CityResponse>> GetAllCities();
    }
}
