﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TP.Front.Application.Models;
using TP.Transverse.Models.Response.PQR;

namespace TP.Front.Application.Services.Interfaces
{
    public interface IPQRService
    {
        Task<GetAllPQRByFilterModel> GetPQRByFilter(PQRByFilterModel model);
        Task<PQRModel> GetPQRByIdentifier(Guid identifier);
        Task<List<PQRModel>> GetAllPQRs();
        Task<List<PQRTypeResponse>> GetAllPQRTypes();
        Task<PQRResponse> CreatePQR(CreatePQRModel request);
    }
}
