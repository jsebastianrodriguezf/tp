﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TP.Front.Application.Models;

namespace TP.Front.Application.Services.Interfaces
{
    public interface IDocumentTypeService
    {
        Task<List<DocumentTypeModel>> GetAllDocumentTypes();
    }
}
