﻿namespace TP.Front.Application.Utility.AppSettingsKeys
{
    public class TPFrontAPIKeys
    {
        public string BaseRoute { get; set; }
        public string GetAllCities { get; set; }
        public string GetCustomer { get; set; }
        public string GetCustomerByDocument { get; set; }
        public string CreateCustomer { get; set; }
        public string UpdateCustomer { get; set; }
        public string GetCustomerAverage { get; set; }
        public string GetCustomerByFilter { get; set; } 
        public string GetAllDocumentTypes { get; set; }
        public string GetAllPQRs { get; set; }
        public string GetPQRByIdentifier { get; set; }
        public string CreatePQR { get; set; }
        public string GetPQRByFilter { get; set; }
        public string GetAllPQRTypes { get; set; }
    }
}
