﻿using AutoMapper;
using TP.Front.Application.Models;
using TP.Transverse.Models.Response.DocumentType;

namespace TP.Front.Application.Utility.Profiles
{
    public class DocumentTypeProfile : Profile
    {
        public DocumentTypeProfile()
        {
            CreateMap<DocumentTypeResponse, DocumentTypeModel>()
                .ForMember(d => d.Code, s => s.MapFrom(src => src.Code))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name));
        }
    }
}
