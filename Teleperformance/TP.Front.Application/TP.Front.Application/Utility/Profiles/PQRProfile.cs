﻿using AutoMapper;
using TP.Front.Application.Models;
using TP.Transverse.Models.Request.PQR;
using TP.Transverse.Models.Response.PQR;

namespace TP.Front.Application.Utility.Profiles
{
    public class PQRProfile : Profile
    {
        public PQRProfile()
        {
            CreateMap<CreatePQRModel, CreatePQRRequest>()
                .ForMember(d => d.Comment, s => s.MapFrom(src => src.Comment))
                .ForMember(d => d.IdCustomer, s => s.MapFrom(src => src.IdCustomer))
                .ForMember(d => d.PQRTypeCode, s => s.MapFrom(src => src.PQRTypeCode));

            CreateMap<PQRResponse, PQRModel>()
                .ForMember(d => d.Comment, s => s.MapFrom(src => src.Comment))
                .ForMember(d => d.IdCustomer, s => s.MapFrom(src => src.IdCustomer))
                .ForMember(d => d.PQRTypeCode, s => s.MapFrom(src => src.PQRTypeCode))
                .ForMember(d => d.CreationDate, s => s.MapFrom(src => src.CreationDate))
                .ForMember(d => d.FullNameCustomer, s => s.MapFrom(src => src.FullNameCustomer))
                .ForMember(d => d.Identifier, s => s.MapFrom(src => src.Identifier))
                .ForMember(d => d.PQRTypeName, s => s.MapFrom(src => src.PQRTypeName));

            CreateMap<PQRByFilterModel, GetPQRByFilterRequest>()
                .ForMember(d => d.DocumentNumber, s => s.MapFrom(src => src.DocumentNumber))
                .ForMember(d => d.From, s => s.MapFrom(src => src.From))
                .ForMember(d => d.IdCity, s => s.MapFrom(src => src.IdCity))
                .ForMember(d => d.Until, s => s.MapFrom(src => src.Until));
        }
    }
}
