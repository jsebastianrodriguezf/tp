﻿using AutoMapper;
using TP.Front.Application.Models;
using TP.Transverse.Models.Request.Customer;

namespace TP.Front.Application.Utility.Profiles
{
    public class CustomerProfile : Profile
    {
        public CustomerProfile()
        {
            CreateMap<CustomerByDocumentModel, GetCustomerByDocumentRequest>()
                .ForMember(d => d.DocumentCode, s => s.MapFrom(src => src.DocumentTypeCode))
                .ForMember(d => d.DocumentNumber, s => s.MapFrom(src => src.DocumentNumber));

            CreateMap<CreateCustomerModel, CreateCustomerRequest>()
                .ForMember(d => d.DocumentNumber, s => s.MapFrom(src => src.DocumentNumber))
                .ForMember(d => d.Birthday, s => s.MapFrom(src => src.Birthday))
                .ForMember(d => d.DocumentTypeCode, s => s.MapFrom(src => src.DocumentTypeCode))
                .ForMember(d => d.FullName, s => s.MapFrom(src => src.FullName))
                .ForMember(d => d.IdCity, s => s.MapFrom(src => src.IdCity))
                .ForMember(d => d.PhoneNumber, s => s.MapFrom(src => src.PhoneNumber));

            CreateMap<UpdateCustomerModel, UpdateCustomerRequest>()
                .ForMember(d => d.IdCity, s => s.MapFrom(src => src.IdCity))
                .ForMember(d => d.IdCustomer, s => s.MapFrom(src => src.IdCustomer))
                .ForMember(d => d.PhoneNumber, s => s.MapFrom(src => src.PhoneNumber));

            CreateMap<CustomerByFilterModel, CustomerByFilterRequest>()
                .ForMember(d => d.DocumentNumber, s => s.MapFrom(src => src.DocumentNumber))
                .ForMember(d => d.From, s => s.MapFrom(src => src.From))
                .ForMember(d => d.IdCity, s => s.MapFrom(src => src.IdCity))
                .ForMember(d => d.Until, s => s.MapFrom(src => src.Until));
        }
    }
}
