﻿using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using TP.Front.Application.Utility.AppSettingsKeys;
using TP.Front.Application.Repository.Interfaces;
using TP.Front.Application.Utility.ExternalServices;
using TP.Transverse.Models.Request.Customer;
using TP.Transverse.Models.Response;
using System.Net.Http;

namespace TP.Front.Application.Repository.Implementations
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly IConsumeExternalService _consumeExternalService;
        private readonly IOptions<TPFrontAPIKeys> _tpFrontApiKeys;

        public CustomerRepository(IConsumeExternalService consumeExternalService, IOptions<TPFrontAPIKeys> tpFrontApiKeys)
        {
            _consumeExternalService = consumeExternalService;
            _tpFrontApiKeys = tpFrontApiKeys;
        }

        public async Task<BaseApiResponseWithData> CreateCustomer(CreateCustomerRequest request)
        {
            string url = _tpFrontApiKeys.Value.CreateCustomer;
            BaseApiResponseWithData baseApiResponse = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Post, request);

            return baseApiResponse;
        }

        public async Task<BaseApiResponseWithData> GetCustomer(long idCustomer)
        {
            string url = string.Format(_tpFrontApiKeys.Value.GetCustomer, idCustomer);
            BaseApiResponseWithData baseApiResponse = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Get);

            return baseApiResponse;
        }

        public async Task<BaseApiResponseWithData> GetCustomerByDocument(GetCustomerByDocumentRequest request)
        {
            string url = _tpFrontApiKeys.Value.GetCustomerByDocument;
            BaseApiResponseWithData baseApiResponse = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Post, request);

            return baseApiResponse;
        }

        public async Task<BaseApiResponseWithData> UpdateCustomer(UpdateCustomerRequest request)
        {
            string url = _tpFrontApiKeys.Value.UpdateCustomer;
            BaseApiResponseWithData baseApiResponse = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Put, request);

            return baseApiResponse;
        }

        public async Task<BaseApiResponseWithData> GetCustomerAverage()
        {
            string url = _tpFrontApiKeys.Value.GetCustomerAverage;
            BaseApiResponseWithData baseApiResponse = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Get);

            return baseApiResponse;
        }

        public async Task<BaseApiResponseWithData> GetCustomerByFilter(CustomerByFilterRequest request)
        {
            string url = _tpFrontApiKeys.Value.GetCustomerByFilter;
            BaseApiResponseWithData baseApiResponse = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Post, request);

            return baseApiResponse;
        }

    }
}
