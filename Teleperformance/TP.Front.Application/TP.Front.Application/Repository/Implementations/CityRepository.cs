﻿using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Threading.Tasks;
using TP.Front.Application.Repository.Interfaces;
using TP.Front.Application.Utility.AppSettingsKeys;
using TP.Front.Application.Utility.ExternalServices;
using TP.Transverse.Models.Response;

namespace TP.Front.Application.Repository.Implementations
{
    public class CityRepository : ICityRepository
    {
        private readonly IConsumeExternalService _consumeExternalService;
        private readonly IOptions<TPFrontAPIKeys> _tpFrontApiKeys;

        public CityRepository(IConsumeExternalService consumeExternalService, IOptions<TPFrontAPIKeys> tpFrontApiKeys)
        {
            _consumeExternalService = consumeExternalService;
            _tpFrontApiKeys = tpFrontApiKeys;
        }

        public async Task<BaseApiResponseWithData> GetAllCities()
        {
            string url = _tpFrontApiKeys.Value.GetAllCities;
            BaseApiResponseWithData baseApiResponse = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Get);

            return baseApiResponse;
        }
    }
}
