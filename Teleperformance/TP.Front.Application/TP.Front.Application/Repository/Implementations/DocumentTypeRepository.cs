﻿using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using TP.Front.Application.Repository.Interfaces;
using TP.Front.Application.Utility.AppSettingsKeys;
using TP.Front.Application.Utility.ExternalServices;
using TP.Transverse.Models.Response;

namespace TP.Front.Application.Repository.Implementations
{
    public class DocumentTypeRepository : IDocumentTypeRepository
    {
        private readonly IConsumeExternalService _consumeExternalService;
        private readonly IOptions<TPFrontAPIKeys> _tpFrontApiKeys;

        public DocumentTypeRepository(IConsumeExternalService consumeExternalService, IOptions<TPFrontAPIKeys> tpFrontApiKeys)
        {
            _consumeExternalService = consumeExternalService;
            _tpFrontApiKeys = tpFrontApiKeys;
        }

        public async Task<BaseApiResponseWithData> GetAllDocumentTypes()
        {
            string url = _tpFrontApiKeys.Value.GetAllDocumentTypes;
            BaseApiResponseWithData baseApiResponse = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Get);

            return baseApiResponse;
        }
    }
}
