﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using TP.Front.Application.Utility.AppSettingsKeys;
using TP.Front.Application.Repository.Interfaces;
using TP.Front.Application.Utility.ExternalServices;
using TP.Transverse.Models.Request.PQR;
using TP.Transverse.Models.Response;
using System.Net.Http;

namespace TP.Front.Application.Repository.Implementations
{
    public class PQRRepository : IPQRRepository
    {
        private readonly IConsumeExternalService _consumeExternalService;
        private readonly IOptions<TPFrontAPIKeys> _tpFrontApiKeys;

        public PQRRepository(IConsumeExternalService consumeExternalService, IOptions<TPFrontAPIKeys> tpFrontApiKeys)
        {
            _consumeExternalService = consumeExternalService;
            _tpFrontApiKeys = tpFrontApiKeys;
        }

        public async Task<BaseApiResponseWithData> CreatePQR(CreatePQRRequest request)
        {
            string url = _tpFrontApiKeys.Value.CreatePQR;
            BaseApiResponseWithData BaseApiResponseWithData = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Post, request);

            return BaseApiResponseWithData;
        }

        public async Task<BaseApiResponseWithData> GetAllPQRs()
        {
            string url = _tpFrontApiKeys.Value.GetAllPQRs;
            BaseApiResponseWithData BaseApiResponseWithData = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Get);

            return BaseApiResponseWithData;
        }

        public async Task<BaseApiResponseWithData> GetPQRAverages(GetPQRAveragesRequest request)
        {
            string url = string.Empty;//_tpFrontApiKeys.Value.GetAveragePQRs;
            BaseApiResponseWithData BaseApiResponseWithData = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Post, request);

            return BaseApiResponseWithData;
        }

        public async Task<BaseApiResponseWithData> GetPQRByFilter(GetPQRByFilterRequest request)
        {
            string url = _tpFrontApiKeys.Value.GetPQRByFilter;
            BaseApiResponseWithData BaseApiResponseWithData = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Post, request);

            return BaseApiResponseWithData;
        }

        public async Task<BaseApiResponseWithData> GetPQRByIdentifier(Guid identifier)
        {
            string url = string.Format(_tpFrontApiKeys.Value.GetPQRByIdentifier, identifier);
            BaseApiResponseWithData BaseApiResponseWithData = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Get);

            return BaseApiResponseWithData;
        }

        public async Task<BaseApiResponseWithData> GetAllPQRTypes()
        {
            string url = _tpFrontApiKeys.Value.GetAllPQRTypes;
            BaseApiResponseWithData BaseApiResponseWithData = await _consumeExternalService.RestAsync<BaseApiResponseWithData>(url, HttpMethod.Get);

            return BaseApiResponseWithData;
        }
    }
}
