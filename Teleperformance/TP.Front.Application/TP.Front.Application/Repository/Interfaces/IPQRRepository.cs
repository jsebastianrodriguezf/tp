﻿using System;
using System.Threading.Tasks;
using TP.Transverse.Models.Response;
using TP.Transverse.Models.Request.PQR;

namespace TP.Front.Application.Repository.Interfaces
{
    public interface IPQRRepository
    {
        Task<BaseApiResponseWithData> CreatePQR(CreatePQRRequest request);
        Task<BaseApiResponseWithData> GetPQRByIdentifier(Guid identifier);
        Task<BaseApiResponseWithData> GetPQRByFilter(GetPQRByFilterRequest request);
        Task<BaseApiResponseWithData> GetPQRAverages(GetPQRAveragesRequest request);
        Task<BaseApiResponseWithData> GetAllPQRs();
        Task<BaseApiResponseWithData> GetAllPQRTypes();
    }
}
