﻿using System.Threading.Tasks;
using TP.Transverse.Models.Response;

namespace TP.Front.Application.Repository.Interfaces
{
    public interface ICityRepository
    {
        Task<BaseApiResponseWithData> GetAllCities();
    }
}
