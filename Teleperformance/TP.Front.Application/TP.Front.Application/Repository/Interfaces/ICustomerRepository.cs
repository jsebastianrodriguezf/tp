﻿using System.Threading.Tasks;
using TP.Transverse.Models.Response;
using TP.Transverse.Models.Request.Customer;

namespace TP.Front.Application.Repository.Interfaces
{
    public interface ICustomerRepository
    {
        Task<BaseApiResponseWithData> GetCustomer(long idCustomer);
        Task<BaseApiResponseWithData> CreateCustomer(CreateCustomerRequest request);
        Task<BaseApiResponseWithData> GetCustomerByDocument(GetCustomerByDocumentRequest request);
        Task<BaseApiResponseWithData> UpdateCustomer(UpdateCustomerRequest request);
        Task<BaseApiResponseWithData> GetCustomerAverage();
        Task<BaseApiResponseWithData> GetCustomerByFilter(CustomerByFilterRequest customerFilter);
    }
}
