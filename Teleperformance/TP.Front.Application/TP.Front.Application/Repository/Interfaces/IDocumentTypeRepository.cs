﻿using System.Threading.Tasks;
using TP.Transverse.Models.Response;

namespace TP.Front.Application.Repository.Interfaces
{
    public interface IDocumentTypeRepository
    {
        Task<BaseApiResponseWithData> GetAllDocumentTypes();
    }
}
