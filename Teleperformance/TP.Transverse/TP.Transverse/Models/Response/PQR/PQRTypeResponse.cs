﻿namespace TP.Transverse.Models.Response.PQR
{
    public class PQRTypeResponse
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
