﻿using static TP.Transverse.Models.Response.ResponseCode;

namespace TP.Transverse.Models.Response
{
    public class BaseSuccessApiResponseWithData : BaseApiResponse
    {
        public override string Status { get; set; }
        public override int StatusCode { get; set; }
        public override string Message { get; set; }
        public object Data { get; set; }

        public BaseSuccessApiResponseWithData()
        {
            Status = ResponseCode.Status[StatusCodeEnum.OK];
            StatusCode = (int)StatusCodeEnum.OK;
            Message = ResponseCode.StatusMessage[StatusCodeEnum.OK];
        }
    }
}
