﻿namespace TP.Transverse.Models.Response.DocumentType
{
    public class DocumentTypeResponse
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
