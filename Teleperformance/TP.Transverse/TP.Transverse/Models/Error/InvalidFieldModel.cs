﻿using System.Collections.Generic;

namespace TP.Transverse.Models.Response.Error
{
    public class InvalidFieldModel
    {
        public string Field { get; set; }
        public List<string> Errors { get; set; }
    }
}
