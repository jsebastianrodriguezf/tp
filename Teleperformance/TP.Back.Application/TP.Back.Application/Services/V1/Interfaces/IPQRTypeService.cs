﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TP.Back.Application.Models;
using TP.Back.Database.Entities;

namespace TP.Back.Application.Services.V1.Interfaces
{
    public interface IPQRTypeService
    {
        Task<PQRType> GetPQRTypeByCode(string PQRTypeCode);
        Task<List<PQRType>> GetAllPQRTypes();
    }
}
