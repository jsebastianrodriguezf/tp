﻿using System;
using System.Threading.Tasks;
using TP.Transverse.Models.Request.PQR;

namespace TP.Back.Application.Services.V1.Interfaces
{
    public interface IPQRService
    {
        Task<object> GetAllPQRs();
        Task<object> GetPQRByIdentifier(Guid identifier);
        Task<object> CreatePQR(CreatePQRRequest request);
        Task<object> GetPQRByFilter(GetPQRByFilterRequest request);
        Task<object> GetAllPQRTypes();
    }
}
