﻿using System.Threading.Tasks;
using TP.Back.Database.Entities;

namespace TP.Back.Application.Services.V1.Interfaces
{
    public interface IDocumentTypeService
    {
        Task<object> GetAllDocumentTypes();
        Task<DocumentType> GetDocumentByCode(string documentTypeCode);
    }
}
