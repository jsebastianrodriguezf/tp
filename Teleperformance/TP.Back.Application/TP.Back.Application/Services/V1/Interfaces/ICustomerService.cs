﻿using System.Threading.Tasks;
using TP.Transverse.Models.Request.Customer;

namespace TP.Back.Application.Services.V1.Interfaces
{
    public interface ICustomerService
    {
        Task<object> Get(long idCustomer);
        Task<object> GetCustomerByDocument(GetCustomerByDocumentRequest request);
        Task<object> CreateCustomer(CreateCustomerRequest request);
        Task<object> UpdateCustomer(UpdateCustomerRequest request);
        Task<object> GetCustomerAverage();
        Task<object> GetCustomerByFilter(CustomerByFilterRequest request);
    }
}
