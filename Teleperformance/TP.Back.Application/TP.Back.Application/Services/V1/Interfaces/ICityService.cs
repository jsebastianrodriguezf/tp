﻿using System.Threading.Tasks;

namespace TP.Back.Application.Services.V1.Interfaces
{
    public interface ICityService
    {
        Task<object> GetAllCities();
    }
}
