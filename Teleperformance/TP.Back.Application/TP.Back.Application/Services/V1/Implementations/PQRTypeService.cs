﻿using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using TP.Back.Application.Repository.Implementations;
using TP.Back.Application.Services.V1.Interfaces;
using TP.Back.Database.Entities;

namespace TP.Back.Application.Services.V1.Implementations
{
    public class PQRTypeService : IPQRTypeService
    {
        private readonly IMapper _mapper;
        private readonly PQRTypeRepository _PQRTypeRepository;

        public PQRTypeService(IMapper mapper, PQRTypeRepository pQRTypeRepository)
        {
            _PQRTypeRepository = pQRTypeRepository;
            _mapper = mapper;
        }

        public async Task<PQRType> GetPQRTypeByCode(string PQRTypeCode)
        {
            return await _PQRTypeRepository.GetPQRTypeByCode(PQRTypeCode);
        }

        public async Task<List<PQRType>> GetAllPQRTypes()
        {
            return await _PQRTypeRepository.GetAllAsync();
        }
    }
}
