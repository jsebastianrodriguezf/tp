﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TP.Back.Application.Models;
using TP.Back.Application.Repository.Implementations;
using TP.Back.Application.Services.V1.Interfaces;
using TP.Back.Database.Entities;
using TP.Transverse.Helpers;
using TP.Transverse.Models.Request.Customer;
using TP.Transverse.Models.Request.PQR;
using TP.Transverse.Models.Response.PQR;
using static TP.Transverse.Models.Response.ResponseCode;

namespace TP.Back.Application.Services.V1.Implementations
{
    public class PQRService : IPQRService
    {
        private readonly IMapper _mapper;
        private readonly PQRRepository _PQRRepository;
        private readonly IPQRTypeService _pQRTypeService;

        public PQRService(IMapper mapper, PQRRepository pqrRepository, IPQRTypeService pQRTypeService)
        {
            _PQRRepository = pqrRepository;
            _pQRTypeService = pQRTypeService;
            _mapper = mapper;
        }

        public async Task<object> CreatePQR(CreatePQRRequest request)
        {
            PQRType pQRType = await _pQRTypeService.GetPQRTypeByCode(request.PQRTypeCode);
            PQRModel response = await _PQRRepository.CreatePQR(request.Comment, pQRType.Id, request.IdCustomer);

            PQRResponse pqr = _mapper.Map<PQRModel, PQRResponse>(response);

            return ResponseHelper.SetSuccessResponseWithData(pqr);
        }

        public async Task<object> GetAllPQRs()
        {
            List<PQRModel> pQRModels = await _PQRRepository.GetAllPQRs();

            List<PQRResponse> pqrsResponse = _mapper.Map<List<PQRModel>, List<PQRResponse>>(pQRModels);

            return ResponseHelper.SetSuccessResponseWithData(pqrsResponse);
        }

        public async Task<object> GetPQRByFilter(GetPQRByFilterRequest request)
        {
            List<PQRModel> pQRModels = await _PQRRepository.GetPQRsByFilter(request.From, request.Until, request.IdCity, request.DocumentNumber);

            List<PQRResponse> pqrsResponse = _mapper.Map<List<PQRModel>, List<PQRResponse>>(pQRModels);

            return ResponseHelper.SetSuccessResponseWithData(pqrsResponse);
        }

        public async Task<object> GetPQRByIdentifier(Guid identifier)
        {
            PQRModel response = await _PQRRepository.GetByIdentifier(identifier);

            if (response == null)
                return ResponseHelper.SetResponse(StatusCodeEnum.NOT_FOUND, Status[StatusCodeEnum.NOT_FOUND], StatusMessage[StatusCodeEnum.NOT_FOUND]);

            PQRResponse pqr = _mapper.Map<PQRModel, PQRResponse>(response);

            return ResponseHelper.SetSuccessResponseWithData(pqr);
        }

        public async Task<object> GetAllPQRTypes()
        {
            List<PQRType> PQRTypes = await _pQRTypeService.GetAllPQRTypes();

            List<PQRTypeResponse> PQRTypesResponse = _mapper.Map<List<PQRType>, List<PQRTypeResponse>>(PQRTypes);

            return ResponseHelper.SetSuccessResponseWithData(PQRTypesResponse);
        }
    }
}
