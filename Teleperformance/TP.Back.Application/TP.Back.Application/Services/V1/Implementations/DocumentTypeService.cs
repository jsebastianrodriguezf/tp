﻿using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using TP.Back.Application.Repository.Implementations;
using TP.Back.Application.Services.V1.Interfaces;
using TP.Back.Database.Entities;
using TP.Transverse.Helpers;
using TP.Transverse.Models.Response.DocumentType;

namespace TP.Back.Application.Services.V1.Implementations
{
    public class DocumentTypeService : IDocumentTypeService
    {
        private readonly IMapper _mapper;
        private readonly DocumentTypeRepository _documentTypeRepository;

        public DocumentTypeService(IMapper mapper, DocumentTypeRepository documentTypeRepository)
        {
            _mapper = mapper;
            _documentTypeRepository = documentTypeRepository;
        }

        public async Task<object> GetAllDocumentTypes()
        {
            List<DocumentType> documentTypes = await _documentTypeRepository.GetAllAsync();
            List<DocumentTypeResponse> documentTypesResponses = _mapper.Map<List<DocumentType>, List<DocumentTypeResponse>>(documentTypes);

            return ResponseHelper.SetSuccessResponseWithData(documentTypesResponses);
        }

        public async Task<DocumentType> GetDocumentByCode(string documentTypeCode)
        {
            return await _documentTypeRepository.GetDocumentTypeByCode(documentTypeCode);
        }
    }
}
