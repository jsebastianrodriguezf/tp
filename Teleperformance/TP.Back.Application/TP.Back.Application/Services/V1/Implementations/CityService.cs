﻿using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using TP.Back.Application.Repository.Implementations;
using TP.Back.Application.Services.V1.Interfaces;
using TP.Back.Database.Entities;
using TP.Transverse.Helpers;
using TP.Transverse.Models.Response.City;

namespace TP.Back.Application.Services.V1.Implementations
{
    public class CityService : ICityService
    {
        private readonly IMapper _mapper;
        private readonly CityRepository _cityRepository;

        public CityService(IMapper mapper, CityRepository cityRepository)
        {
            _cityRepository = cityRepository;
            _mapper = mapper;
        }

        public async Task<object> GetAllCities()
        {
            List<City> cities = await _cityRepository.GetAllAsync();
            List<CityResponse> cityResponses =  _mapper.Map<List<City>, List<CityResponse>>(cities);

            return ResponseHelper.SetSuccessResponseWithData(cityResponses);
        }
    }
}
