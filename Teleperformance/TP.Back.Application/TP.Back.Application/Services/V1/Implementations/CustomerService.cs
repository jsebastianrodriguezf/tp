﻿using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TP.Back.Application.Models;
using TP.Back.Application.Repository.Implementations;
using TP.Back.Application.Services.V1.Interfaces;
using TP.Back.Database.Entities;
using TP.Transverse.Helpers;
using TP.Transverse.Models.Request.Customer;
using TP.Transverse.Models.Response.Customer;
using static TP.Transverse.Models.Response.ResponseCode;

namespace TP.Back.Application.Services.V1.Implementations
{
    public class CustomerService : ICustomerService
    {
        private readonly IMapper _mapper;
        private readonly CustomerRepository _customerRepository;
        private readonly IDocumentTypeService _documentTypeService;

        public CustomerService(IMapper mapper, CustomerRepository customerRepository, IDocumentTypeService documentTypeService)
        {
            _customerRepository = customerRepository;
            _documentTypeService = documentTypeService;
            _mapper = mapper;
        }

        public async Task<object> Get(long idCustomer)
        {
            CustomerModel response = await _customerRepository.Get(idCustomer);

            if (response == null)
                return ResponseHelper.SetResponse(StatusCodeEnum.NOT_FOUND, Status[StatusCodeEnum.NOT_FOUND], StatusMessage[StatusCodeEnum.NOT_FOUND]);

            CustomerResponse getCustomer = _mapper.Map<CustomerModel, CustomerResponse>(response);

            return ResponseHelper.SetSuccessResponseWithData(getCustomer);
        }

        public async Task<object> GetCustomerByFilter(CustomerByFilterRequest request)
        {
            List<CustomerModel> response = await _customerRepository.GetCustomerByFilter(request.From, request.Until, request.IdCity, request.DocumentNumber);

            List<CustomerResponse> customers = _mapper.Map<List<CustomerModel>, List<CustomerResponse>>(response);

            return ResponseHelper.SetSuccessResponseWithData(customers);
        }

        public async Task<object> CreateCustomer(CreateCustomerRequest request)
        {
            DocumentType documentType = await _documentTypeService.GetDocumentByCode(request.DocumentTypeCode);
            CustomerModel customerModel = await _customerRepository.CreateCustomer(request.FullName, documentType.Id, request.DocumentNumber, request.Birthday, request.IdCity, request.PhoneNumber);

            CustomerResponse getCustomer = _mapper.Map<CustomerModel, CustomerResponse>(customerModel);

            return ResponseHelper.SetSuccessResponseWithData(getCustomer);
        }

        public async Task<object> GetCustomerByDocument(GetCustomerByDocumentRequest request)
        {
            CustomerModel response = await _customerRepository.GetCustomerByDocument(request.DocumentCode, request.DocumentNumber);

            if (response == null)
                return ResponseHelper.SetResponse(StatusCodeEnum.NOT_FOUND, Status[StatusCodeEnum.NOT_FOUND], StatusMessage[StatusCodeEnum.NOT_FOUND]);

            CustomerResponse getCustomer = _mapper.Map<CustomerModel, CustomerResponse>(response);

            return ResponseHelper.SetSuccessResponseWithData(getCustomer);
        }

        public async Task<object> UpdateCustomer(UpdateCustomerRequest request)
        {
            CustomerModel customerModel = await _customerRepository.UpdateCustomer(request.IdCustomer, string.Empty, default(int), string.Empty, DateTime.Now, request.IdCity, request.PhoneNumber);

            CustomerResponse getCustomer = _mapper.Map<CustomerModel, CustomerResponse>(customerModel);

            return ResponseHelper.SetSuccessResponseWithData(getCustomer);
        }

        public async Task<object> GetCustomerAverage()
        {
            CustomersAverageModel customersAverage = await _customerRepository.GetCustomersAverage();

            CustomersAverageResponse averageResponse = _mapper.Map<CustomersAverageModel, CustomersAverageResponse>(customersAverage);

            return ResponseHelper.SetSuccessResponseWithData(averageResponse);
        }
    }
}
