﻿namespace TP.Back.Application.Models
{
    public class CustomersAverageModel
    {
        public int Average { get; set; }
        public int YoungCustomers { get; set; }
        public int OldCustomers { get; set; }
    }
}
