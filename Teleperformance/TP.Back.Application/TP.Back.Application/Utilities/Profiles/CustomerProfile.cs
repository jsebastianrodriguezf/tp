﻿using AutoMapper;
using TP.Back.Application.Models;
using TP.Transverse.Models.Response.Customer;

namespace TP.Back.Application.Utilities.Profiles
{
    public class CustomerProfile : Profile
    {
        public CustomerProfile()
        {
            CreateMap<CustomerModel, CustomerResponse>()
                .ForMember(d => d.Birthday, s => s.MapFrom(src => src.Birthday))
                .ForMember(d => d.CityName, s => s.MapFrom(src => src.CityName))
                .ForMember(d => d.DocumentNumber, s => s.MapFrom(src => src.DocumentNumber))
                .ForMember(d => d.DocumentTypeCode, s => s.MapFrom(src => src.DocumentTypeCode))
                .ForMember(d => d.FullName, s => s.MapFrom(src => src.FullName))
                .ForMember(d => d.IdCity, s => s.MapFrom(src => src.IdCity))
                .ForMember(d => d.IdCustomer, s => s.MapFrom(src => src.IdCustomer))
                .ForMember(d => d.PhoneNumber, s => s.MapFrom(src => src.PhoneNumber))
                .ForMember(d => d.DocumentTypeName, s => s.MapFrom(src => src.DocumentTypeName));

            CreateMap<CustomersAverageModel, CustomersAverageResponse>()
                .ForMember(d => d.NumberOldCustomers, s => s.MapFrom(src => src.OldCustomers))
                .ForMember(d => d.NumberYoungCustomers, s => s.MapFrom(src => src.YoungCustomers))
                .ForMember(d => d.OldAverage, s => s.MapFrom(src => src.Average));
        }
    }
}
