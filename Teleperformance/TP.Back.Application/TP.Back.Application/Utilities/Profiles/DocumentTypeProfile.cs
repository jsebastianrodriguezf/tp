﻿using AutoMapper;
using TP.Back.Database.Entities;
using TP.Transverse.Models.Response.DocumentType;

namespace TP.Back.Application.Utilities.Profiles
{
    public class DocumentTypeProfile : Profile
    {
        public DocumentTypeProfile()
        {
            CreateMap<DocumentType, DocumentTypeResponse>()
                .ForMember(d => d.Code, s => s.MapFrom(src => src.Code))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name));
        }
    }
}
