﻿using AutoMapper;
using TP.Back.Application.Models;
using TP.Back.Database.Entities;
using TP.Transverse.Models.Response.PQR;

namespace TP.Back.Application.Utilities.Profiles
{
    public class PQRProfile : Profile
    {
        public PQRProfile()
        {
            CreateMap<PQRModel, PQRResponse>()
                .ForMember(d => d.Comment, s => s.MapFrom(src => src.Comment))
                .ForMember(d => d.CreationDate, s => s.MapFrom(src => src.CreationDate))
                .ForMember(d => d.FullNameCustomer, s => s.MapFrom(src => src.FullNameCustomer))
                .ForMember(d => d.IdCustomer, s => s.MapFrom(src => src.IdCustomer))
                .ForMember(d => d.Identifier, s => s.MapFrom(src => src.Identifier))
                .ForMember(d => d.PQRTypeCode, s => s.MapFrom(src => src.PQRTypeCode))
                .ForMember(d => d.PQRTypeName, s => s.MapFrom(src => src.PQRTypeName));

            CreateMap<PQRType, PQRTypeResponse>()
                .ForMember(d => d.Code, s => s.MapFrom(src => src.Code))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name));
        }
    }
}
