﻿using AutoMapper;
using TP.Back.Database.Entities;
using TP.Transverse.Models.Response.City;

namespace TP.Back.Application2.Utilities.Profiles
{
    public class CityProfile : Profile
    {
        public CityProfile()
        {
            CreateMap<City, CityResponse>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name));
        }
    }
}
