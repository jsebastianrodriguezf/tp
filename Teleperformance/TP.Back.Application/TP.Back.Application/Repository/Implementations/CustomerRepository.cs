﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using TP.Back.Application.Models;
using TP.Back.Database.Context;
using TP.Back.Database.Entities;
using TP.Back.Database.Repository;

namespace TP.Back.Application.Repository.Implementations
{
    public class CustomerRepository : BaseRepository<Customer, TPContext>
    {
        private readonly TPContext _context;

        public CustomerRepository(TPContext context) : base(context)
        {
            _context = context;
        }

        public async Task<CustomerModel> GetCustomerByDocument(string documentTypeCode, string documentNumber)
        {
            var sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@DocumentTypeCode", documentTypeCode),
                new SqlParameter("@DocumentNumber", documentNumber),
                new SqlParameter("@OUT_JSON", SqlDbType.VarChar)
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Size = int.MaxValue,
                        Direction = ParameterDirection.Output,
                        DbType = DbType.String
                    }
            };

            await _context.Database.ExecuteSqlRawAsync("EXEC [TP].[GetCustomerByDocument] @DocumentTypeCode, @DocumentNumber, @OUT_JSON OUTPUT", sqlParameter);

            string response = sqlParameter[^1].Value.ToString();
            if (string.IsNullOrWhiteSpace(response))
                return null;

            return JsonConvert.DeserializeObject<List<CustomerModel>>(response).FirstOrDefault();
        }

        public async Task<List<CustomerModel>> GetCustomerByFilter(DateTime from, DateTime until, int? idCity, string documentNumber)
        {
            var sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@From", from),
                new SqlParameter("@Until", until),
                new SqlParameter("@IdCity", idCity ?? 0),
                new SqlParameter("@DocumentNumber", documentNumber ?? string.Empty),
                new SqlParameter("@OUT_JSON", SqlDbType.VarChar)
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Size = int.MaxValue,
                        Direction = ParameterDirection.Output,
                        DbType = DbType.String
                    }
            };

            await _context.Database.ExecuteSqlRawAsync("EXEC [TP].[GetCustomersByFilter] @From, @Until, @IdCity, @DocumentNumber, @OUT_JSON OUTPUT", sqlParameter);

            string response = sqlParameter[^1].Value.ToString();
            if (string.IsNullOrWhiteSpace(response))
                return new List<CustomerModel>();

            return JsonConvert.DeserializeObject<List<CustomerModel>>(response);
        }

        public async Task<CustomerModel> Get(long idCustomer)
        {
            var sqlParameter = new SqlParameter[]
           {
                new SqlParameter("@IdCustomer", idCustomer),
                new SqlParameter("@OUT_JSON", SqlDbType.VarChar)
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Size = int.MaxValue,
                        Direction = ParameterDirection.Output,
                        DbType = DbType.String
                    }
           };

            await _context.Database.ExecuteSqlRawAsync("EXEC [TP].[GetCustomer] @IdCustomer, @OUT_JSON OUTPUT", sqlParameter);

            string response = sqlParameter[^1].Value.ToString();
            if (string.IsNullOrWhiteSpace(response))
                return null;

            return JsonConvert.DeserializeObject<List<CustomerModel>>(response).FirstOrDefault();
        }

        public async Task<CustomerModel> CreateCustomer(string fullName, int idDocumentType, string documentNumber, DateTime birthday, int idCity, string phoneNumber)
        {
            var sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@FullName", fullName),
                new SqlParameter("@IdDocumentType", idDocumentType),
                new SqlParameter("@DocumentNumber", documentNumber),
                new SqlParameter("@Birthday", birthday),
                new SqlParameter("@IdCity", idCity),
                new SqlParameter("@PhoneNumber", phoneNumber),
                new SqlParameter("@OUT_JSON", SqlDbType.VarChar)
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Size = int.MaxValue,
                        Direction = ParameterDirection.Output,
                        DbType = DbType.String
                    }
            };

            await _context.Database.ExecuteSqlRawAsync("EXEC [TP].[CreateCustomer] @FullName, @IdDocumentType, @DocumentNumber, @Birthday, @IdCity, @PhoneNumber,  @OUT_JSON OUTPUT", sqlParameter);

            string response = sqlParameter[^1].Value.ToString();
            if (string.IsNullOrWhiteSpace(response))
                return null;

            return JsonConvert.DeserializeObject<List<CustomerModel>>(response).FirstOrDefault();
        }

        public async Task<CustomerModel> UpdateCustomer(long idCustomer, string fullName, int idDocumentType, string documentNumber, DateTime birthday, int idCity, string phoneNumber)
        {
            var sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@IdCustomer", idCustomer),
                new SqlParameter("@FullName", fullName),
                new SqlParameter("@IdDocumentType", idDocumentType),
                new SqlParameter("@DocumentNumber", documentNumber),
                new SqlParameter("@Birthday", birthday),
                new SqlParameter("@IdCity", idCity),
                new SqlParameter("@PhoneNumber", phoneNumber),
                new SqlParameter("@OUT_JSON", SqlDbType.VarChar)
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Size = int.MaxValue,
                        Direction = ParameterDirection.Output,
                        DbType = DbType.String
                    }
            };

            await _context.Database.ExecuteSqlRawAsync("EXEC [TP].[UpdateCustomer] @IdCustomer, @FullName, @IdDocumentType, @DocumentNumber, @Birthday, @IdCity, @PhoneNumber,  @OUT_JSON OUTPUT", sqlParameter);

            string response = sqlParameter[^1].Value.ToString();
            if (string.IsNullOrWhiteSpace(response))
                return null;

            return JsonConvert.DeserializeObject<List<CustomerModel>>(response).FirstOrDefault();
        }

        public async Task<CustomersAverageModel> GetCustomersAverage()
        {
            var sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@OUT_JSON", SqlDbType.VarChar)
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Size = int.MaxValue,
                        Direction = ParameterDirection.Output,
                        DbType = DbType.String
                    }
            };

            await _context.Database.ExecuteSqlRawAsync("EXEC [TP].[GetCustomersAverage] @OUT_JSON OUTPUT", sqlParameter);

            string response = sqlParameter[^1].Value.ToString();
            if (string.IsNullOrWhiteSpace(response))
                return null;

            return JsonConvert.DeserializeObject<List<CustomersAverageModel>>(response).FirstOrDefault();
        }
    }
}
