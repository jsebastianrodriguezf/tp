﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TP.Back.Database.Context;
using TP.Back.Database.Entities;
using TP.Back.Database.Repository;

namespace TP.Back.Application.Repository.Implementations
{
    public class PQRTypeRepository : BaseRepository<PQRType, TPContext>
    {
        private readonly TPContext _context;

        public PQRTypeRepository(TPContext context) : base(context)
        {
            _context = context;
        }

        public async Task<PQRType> GetPQRTypeByCode(string code)
        {
            IEnumerable<PQRType> PQRType = await GetFilteredAsync(x => x.Code == code);
            return PQRType.ToList().FirstOrDefault();
        }
    }
}
