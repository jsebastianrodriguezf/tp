﻿using TP.Back.Database.Context;
using TP.Back.Database.Entities;
using TP.Back.Database.Repository;

namespace TP.Back.Application.Repository.Implementations
{
    public class CountryRepository : BaseRepository<Country, TPContext>
    {
        private readonly TPContext _context;

        public CountryRepository(TPContext context) : base(context)
        {
            _context = context;
        }
    }
}
