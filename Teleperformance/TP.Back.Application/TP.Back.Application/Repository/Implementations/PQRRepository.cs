﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using TP.Back.Application.Models;
using TP.Back.Database.Context;
using TP.Back.Database.Entities;
using TP.Back.Database.Repository;

namespace TP.Back.Application.Repository.Implementations
{
    public class PQRRepository : BaseRepository<PQR, TPContext>
    {
        private readonly TPContext _context;

        public PQRRepository(TPContext context) : base(context)
        {
            _context = context;
        }

        public async Task<PQRModel> CreatePQR(string comment, int idPQRType, long idCustomer)
        {
            var sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@Comment", comment),
                new SqlParameter("@IdPQRType", idPQRType),
                new SqlParameter("@IdCustomer", idCustomer),
                new SqlParameter("@OUT_JSON", SqlDbType.VarChar)
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Size = int.MaxValue,
                        Direction = ParameterDirection.Output,
                        DbType = DbType.String
                    }
            };

            await _context.Database.ExecuteSqlRawAsync("EXEC [TP].[CreatePQR] @Comment, @IdPQRType, @IdCustomer, @OUT_JSON OUTPUT", sqlParameter);

            string response = sqlParameter[^1].Value.ToString();
            if (string.IsNullOrWhiteSpace(response))
                return null;

            return JsonConvert.DeserializeObject<List<PQRModel>>(response).FirstOrDefault();
        }

        public async Task<List<PQRModel>> GetPQRsByFilter(DateTime from, DateTime until, int? idCity, string documentNumber)
        {
            var sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@From", from),
                new SqlParameter("@Until", until),
                new SqlParameter("@IdCity", idCity ?? 0),
                new SqlParameter("@DocumentNumber", documentNumber ?? string.Empty),
                new SqlParameter("@OUT_JSON", SqlDbType.VarChar)
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Size = int.MaxValue,
                        Direction = ParameterDirection.Output,
                        DbType = DbType.String
                    }
            };

            await _context.Database.ExecuteSqlRawAsync("EXEC [TP].[GetPQRsByFilter] @From, @Until, @IdCity, @DocumentNumber, @OUT_JSON OUTPUT", sqlParameter);

            string response = sqlParameter[^1].Value.ToString();
            if (string.IsNullOrWhiteSpace(response))
                return new List<PQRModel>();

            return JsonConvert.DeserializeObject<List<PQRModel>>(response);
        }

        public async Task<PQRModel> GetByIdentifier(Guid identifier)
        {
            var sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@Identifier", identifier),
                new SqlParameter("@OUT_JSON", SqlDbType.VarChar)
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Size = int.MaxValue,
                        Direction = ParameterDirection.Output,
                        DbType = DbType.String
                    }
            };

            await _context.Database.ExecuteSqlRawAsync("EXEC [TP].[GetPQRsByIdentifier] @identifier, @OUT_JSON OUTPUT", sqlParameter);

            string response = sqlParameter[^1].Value.ToString();
            if (string.IsNullOrWhiteSpace(response))
                return null;

            return JsonConvert.DeserializeObject<List<PQRModel>>(response).FirstOrDefault();
        }

        public async Task<List<PQRModel>> GetAllPQRs()
        {
            var sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@OUT_JSON", SqlDbType.VarChar)
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Size = int.MaxValue,
                        Direction = ParameterDirection.Output,
                        DbType = DbType.String
                    }
            };

            await _context.Database.ExecuteSqlRawAsync("EXEC [TP].[GetAllPQRs] @OUT_JSON OUTPUT", sqlParameter);

            string response = sqlParameter[^1].Value.ToString();
            if (string.IsNullOrWhiteSpace(response))
                return new List<PQRModel>();

            return JsonConvert.DeserializeObject<List<PQRModel>>(response);
        }
    }
}
