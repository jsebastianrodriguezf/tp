﻿using System.Threading.Tasks;
using TP.Back.Database.Context;
using TP.Back.Database.Entities;
using TP.Back.Database.Repository;
using System.Linq;
using System.Collections.Generic;

namespace TP.Back.Application.Repository.Implementations
{
    public class DocumentTypeRepository : BaseRepository<DocumentType, TPContext>
    {
        private readonly TPContext _context;

        public DocumentTypeRepository(TPContext context) : base(context)
        {
            _context = context;
        }

        public async Task<DocumentType> GetDocumentTypeByCode(string code)
        {
            IEnumerable<DocumentType> documentTypes = await GetFilteredAsync(x => x.Code == code);
            return documentTypes.ToList().FirstOrDefault();
        }
    }
}
