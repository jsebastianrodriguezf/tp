﻿using TP.Back.Database.Context;
using TP.Back.Database.Entities;
using TP.Back.Database.Repository;

namespace TP.Back.Application.Repository.Implementations
{
    public class CityRepository : BaseRepository<City, TPContext>
    {
        private readonly TPContext _context;

        public CityRepository(TPContext context) : base(context)
        {
            _context = context;
        }
    }
}
