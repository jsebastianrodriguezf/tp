﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TP.Back.API.ApiRoutes.V1;
using TP.Back.Application.Services.V1.Interfaces;
using TP.Transverse.Models.Request.PQR;

namespace TP.Back.API.Controllers.V1
{
    [ApiController]
    public class PQRController : ControllerBase
    {
        private readonly IPQRService _PQRService;

        public PQRController(IPQRService pqrService)
        {
            _PQRService = pqrService;
        }

        [HttpGet(ApiRoutesV1.PQR.GetAll)]
        public async Task<IActionResult> GetAllPQRs()
        {
            return Ok(await _PQRService.GetAllPQRs());
        }

        [HttpGet(ApiRoutesV1.PQR.Get)]
        public async Task<IActionResult> GetPQRByIdentifier(Guid identifier)
        {
            return Ok(await _PQRService.GetPQRByIdentifier(identifier));
        }

        [HttpPost(ApiRoutesV1.PQR.Create)]
        public async Task<IActionResult> CreatePQR(CreatePQRRequest request)
        {
            return Ok(await _PQRService.CreatePQR(request));
        }

        [HttpPost(ApiRoutesV1.PQR.GetByFilter)]
        public async Task<IActionResult> GetPQRByFilter(GetPQRByFilterRequest request)
        {
            return Ok(await _PQRService.GetPQRByFilter(request));
        }

        [HttpGet(ApiRoutesV1.PQR.GetAllPQRTypes)]
        public async Task<IActionResult> GetAllPQRTypes()
        {
            return Ok(await _PQRService.GetAllPQRTypes());
        }
    }
}
