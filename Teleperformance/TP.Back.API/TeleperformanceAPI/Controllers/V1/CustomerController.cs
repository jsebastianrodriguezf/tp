﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TP.Back.API.ApiRoutes.V1;
using TP.Back.Application.Services.V1.Interfaces;
using TP.Transverse.Models.Request.Customer;

namespace TP.Back.API.Controllers.V1
{
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet(ApiRoutesV1.Customer.Get)]
        public async Task<IActionResult> GetCustomerByDocument(long idCustomer)
        {
            return Ok(await _customerService.Get(idCustomer));
        }

        [HttpPost(ApiRoutesV1.Customer.GetByDocument)]
        public async Task<IActionResult> GetCustomerByDocument(GetCustomerByDocumentRequest request)
        {
            return Ok(await _customerService.GetCustomerByDocument(request));
        }

        [HttpPost(ApiRoutesV1.Customer.Create)]
        public async Task<IActionResult> CreateCustomer(CreateCustomerRequest request)
        {
            return Ok(await _customerService.CreateCustomer(request));
        }

        [HttpPut(ApiRoutesV1.Customer.Update)]
        public async Task<IActionResult> UpdateCustomer(UpdateCustomerRequest request)
        {
            return Ok(await _customerService.UpdateCustomer(request));
        }

        [HttpGet(ApiRoutesV1.Customer.GetAverage)]
        public async Task<IActionResult> GetCustomerAverage()
        {
            return Ok(await _customerService.GetCustomerAverage());
        }

        [HttpPost(ApiRoutesV1.Customer.GetByFilter)]
        public async Task<IActionResult> GetCustomerByFilter(CustomerByFilterRequest request)
        {
            return Ok(await _customerService.GetCustomerByFilter(request));
        }
    }
}
