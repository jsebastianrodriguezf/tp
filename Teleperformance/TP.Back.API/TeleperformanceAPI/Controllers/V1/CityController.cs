﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TP.Back.API.ApiRoutes.V1;
using TP.Back.Application.Services.V1.Interfaces;

namespace TP.Back.API.Controllers.V1
{
    [ApiController]
    public class CityController : ControllerBase
    {
        private readonly ICityService _cityService;

        public CityController(ICityService cityService)
        {
            _cityService = cityService;
        }

        [HttpGet(ApiRoutesV1.City.GetAll)]
        public async Task<IActionResult> GetAllCities()
        {
            return Ok(await _cityService.GetAllCities());
        }
    }
}
