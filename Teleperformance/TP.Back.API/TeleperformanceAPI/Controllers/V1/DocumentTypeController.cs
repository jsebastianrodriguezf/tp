﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TP.Back.API.ApiRoutes.V1;
using TP.Back.Application.Services.V1.Interfaces;

namespace TP.Back.API.Controllers.V1
{
    [ApiController]
    public class DocumentTypeController : ControllerBase
    {
        private readonly IDocumentTypeService _documentTypeService;

        public DocumentTypeController(IDocumentTypeService documentTypeService)
        {
            _documentTypeService = documentTypeService;
        }

        [HttpGet(ApiRoutesV1.DocumentType.GetAll)]
        public async Task<IActionResult> GetAllDocumentTypes()
        {
            return Ok(await _documentTypeService.GetAllDocumentTypes());
        }
    }
}