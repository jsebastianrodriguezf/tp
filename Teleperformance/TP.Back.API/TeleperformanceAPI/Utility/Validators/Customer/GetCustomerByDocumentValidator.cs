﻿using FluentValidation;
using TP.Back.API.Utility.Constants;
using TP.Transverse.Models.Request.Customer;

namespace TP.Back.API.Utility.Validators.Customer
{
    public class GetCustomerByDocumentValidator : AbstractValidator<GetCustomerByDocumentRequest>
    {
        public GetCustomerByDocumentValidator()
        {
            RuleFor(x => x.DocumentCode)
                .NotEmpty().WithMessage(ValidatorMessageConstants.NotEmpty)
                .NotNull().WithMessage(ValidatorMessageConstants.NotNull);

            RuleFor(x => x.DocumentNumber)
                .NotEmpty().WithMessage(ValidatorMessageConstants.NotEmpty)
                .NotNull().WithMessage(ValidatorMessageConstants.NotNull);
        }
    }
}
