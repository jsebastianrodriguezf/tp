﻿using FluentValidation;
using TP.Back.API.Utility.Constants;
using TP.Transverse.Models.Request.Customer;

namespace TP.Back.API.Utility.Validators.Customer
{
    public class UpdateCustomerValidator : AbstractValidator<UpdateCustomerRequest>
    {
        public UpdateCustomerValidator()
        {
            RuleFor(x => x.PhoneNumber);

            RuleFor(x => x.IdCity)
                .NotEmpty().WithMessage(ValidatorMessageConstants.NotEmpty)
                .NotNull().WithMessage(ValidatorMessageConstants.NotNull);
        }
    }
}
