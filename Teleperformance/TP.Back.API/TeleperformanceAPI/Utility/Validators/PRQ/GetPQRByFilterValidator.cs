﻿using FluentValidation;
using TP.Back.API.Utility.Constants;
using TP.Transverse.Models.Request.PQR;

namespace TP.Back.API.Utility.Validators.PRQ
{
    public class GetPQRByFilterValidator : AbstractValidator<GetPQRByFilterRequest>
    {
        public GetPQRByFilterValidator()
        {
            RuleFor(x => x.From)
               .NotEmpty().WithMessage(ValidatorMessageConstants.NotEmpty)
               .NotNull().WithMessage(ValidatorMessageConstants.NotNull);

            RuleFor(x => x.Until)
                .NotEmpty().WithMessage(ValidatorMessageConstants.NotEmpty)
                .NotNull().WithMessage(ValidatorMessageConstants.NotNull);

            RuleFor(x => x.IdCity);

            RuleFor(x => x.DocumentNumber);
        }
    }
}
