﻿namespace TP.Back.API.Utility.Constants
{
    public class SwaggerConstants
    {
        public const string Version = "V1";
        public const string Title = "TP API {0}";
        public const string Descripcion = "TP API";
        public const string NameContact = "Teleperformance";
        public const string EmailContact = "";
        public const string UrlContact = "https://localhost:44310/";
        public const string UrlJsonSwagger = "/swagger/{0}/swagger.json";
    }
}
