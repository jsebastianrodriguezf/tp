﻿namespace TP.Back.API.ApiRoutes.V1
{
    public static class ApiRoutesV1
    {
        public const string Base = "api/v1";

        public static class Customer
        {
            public const string CustomerBase = Base + "/customer";
            public const string Get = CustomerBase + "/get/{idCustomer}";
            public const string GetByDocument = CustomerBase + "/getByDocument";
            public const string GetAll = CustomerBase + "/getAll";
            public const string GetByFilter = CustomerBase + "/getByFilter";
            public const string Create = CustomerBase + "/create";
            public const string Update = CustomerBase + "/update";
            public const string GetAverage = CustomerBase + "/getAverages";
        }

        public static class City
        {
            public const string CityBase = Base + "/city";
            public const string GetAll = CityBase + "/getAll";
        }

        public static class DocumentType
        {
            public const string DocumentTypeBase = Base + "/documentType";
            public const string GetAll = DocumentTypeBase + "/getAll";
        }

        public static class PQR
        {
            public const string PQRBase = Base + "/pqr";
            public const string Create = PQRBase + "/create";
            public const string GetAll = PQRBase + "/getAll";
            public const string Get = PQRBase + "/get/{identifier}";
            public const string GetByFilter = PQRBase + "/getByFilter";
            public const string GetAllPQRTypes = PQRBase + "/getAllPQRTypes";
        }
    }
}
