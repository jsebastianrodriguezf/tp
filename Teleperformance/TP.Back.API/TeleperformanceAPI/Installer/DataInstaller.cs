﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TP.Back.Application.Repository.Implementations;
using TP.Back.Database.Context;

namespace TP.Back.API.Installer
{
    public class DataInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<TPContext>(
                options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<PQRRepository>();
            services.AddTransient<PQRTypeRepository>();
            services.AddTransient<CustomerRepository>();
            services.AddTransient<CityRepository>();
            services.AddTransient<CountryRepository>();
            services.AddTransient<DocumentTypeRepository>();
        }
    }
}
