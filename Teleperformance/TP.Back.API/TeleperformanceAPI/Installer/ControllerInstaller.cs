﻿using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TP.Back.API.Utility.ActionFilters;
using TP.Back.Application.Services.V1.Implementations;
using TP.Back.Application.Services.V1.Interfaces;

namespace TP.Back.API.Installer
{
    public class ControllerInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddControllers();

            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(FluentValidationActionFilter));
            })
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<IPQRService, PQRService>();
            services.AddScoped<IDocumentTypeService, DocumentTypeService>();
            services.AddScoped<IPQRTypeService, PQRTypeService>();
        }
    }
}
