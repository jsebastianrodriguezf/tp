﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TP.Back.Application.Utilities.Profiles;
using TP.Back.Application2.Utilities.Profiles;

namespace TP.Back.API.Installer
{
    public class AutoMapperInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper(typeof(CustomerProfile));
            services.AddAutoMapper(typeof(CityProfile));
            services.AddAutoMapper(typeof(DocumentTypeProfile));
            services.AddAutoMapper(typeof(PQRProfile));
        }
    }
}
