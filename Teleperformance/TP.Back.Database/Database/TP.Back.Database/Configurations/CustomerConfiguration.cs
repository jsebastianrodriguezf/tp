﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TP.Back.Database.Entities;

namespace TP.Back.Database.Configurations
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.ToTable("Customer", "TP");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .HasColumnType("BIGINT")
                .ValueGeneratedOnAdd();

            builder.Property(e => e.FullName)
                .HasColumnType("VARCHAR")
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(e => e.IdDocumentType)
                .HasColumnType("INT")
                .IsRequired();

            builder.Property(e => e.DocumentNumber)
                .HasColumnType("VARCHAR")
                .IsRequired()
                .HasMaxLength(30);

            builder.Property(e => e.PhoneNumber)
                .HasColumnType("VARCHAR")
                .HasMaxLength(20);

            builder.Property(e => e.Birthday)
                .HasColumnType("DATE")
                .IsRequired();

            builder.Property(e => e.IdCity)
                .HasColumnType("INT")
                .IsRequired();

            builder.HasOne(d => d.DocumentType)
                .WithMany(p => p.Customers)
                .HasForeignKey(d => d.IdDocumentType)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Customer_DocumentType");

            builder.HasOne(d => d.City)
                .WithMany(p => p.Customers)
                .HasForeignKey(d => d.IdCity)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Customer_City");
        }
    }
}
