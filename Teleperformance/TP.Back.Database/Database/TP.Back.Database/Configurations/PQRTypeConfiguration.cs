﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TP.Back.Database.Entities;

namespace TP.Back.Database.Configurations
{
    public class PQRTypeConfiguration : IEntityTypeConfiguration<PQRType>
    {
        public void Configure(EntityTypeBuilder<PQRType> builder)
        {
            builder.ToTable("PQRType", "TP");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .HasColumnType("INT")
                .ValueGeneratedOnAdd();

            builder.Property(e => e.Code)
                .HasColumnType("VARCHAR")
                .IsRequired()
                .HasMaxLength(5);

            builder.Property(e => e.Name)
                .HasColumnType("VARCHAR")
                .IsRequired()
                .HasMaxLength(20);
        }
    }
}
