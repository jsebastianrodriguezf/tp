﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TP.Back.Database.Entities;

namespace TP.Back.Database.Configurations
{
    public class CountryConfiguration : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.ToTable("Country", "TP");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .HasColumnType("INT")
                .ValueGeneratedOnAdd();

            builder.Property(e => e.Name)
                .HasColumnType("VARCHAR")
                .IsRequired()
                .HasMaxLength(30);
        }
    }
}
