﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TP.Back.Database.Entities;

namespace TP.Back.Database.Configurations
{
    public class CityConfiguration : IEntityTypeConfiguration<City>
    {
        public void Configure(EntityTypeBuilder<City> builder)
        {
            builder.ToTable("City", "TP");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .HasColumnType("INT")
                .ValueGeneratedOnAdd();

            builder.Property(e => e.Name)
                .HasColumnType("VARCHAR")
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(e => e.IdCountry)
                .HasColumnType("INT")
                .IsRequired();

            builder.HasOne(d => d.Country)
                .WithMany(p => p.Cities)
                .HasForeignKey(d => d.IdCountry)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_City_Country");
        }
    }
}
