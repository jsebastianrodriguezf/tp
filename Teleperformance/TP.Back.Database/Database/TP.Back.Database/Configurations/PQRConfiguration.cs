﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TP.Back.Database.Entities;

namespace TP.Back.Database.Configurations
{
    public class PQRConfiguration : IEntityTypeConfiguration<PQR>
    {
        public void Configure(EntityTypeBuilder<PQR> builder)
        {
            builder.ToTable("PQR", "TP");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .HasColumnType("BIGINT")
                .ValueGeneratedOnAdd();

            builder.Property(e => e.Identifier)
                .HasColumnType("UNIQUEIDENTIFIER")
                .IsRequired();

            builder.Property(e => e.Comment)
                .HasColumnType("NVARCHAR")
                .HasMaxLength(800);

            builder.Property(e => e.CreationDate)
                .HasColumnType("DATETIME")
                .IsRequired();

            builder.Property(e => e.IdPQRType)
                .HasColumnType("INT")
                .IsRequired();

            builder.Property(e => e.IdCustomer)
                .HasColumnType("BIGINT")
                .IsRequired();

            builder.HasOne(d => d.PQRType)
                .WithMany(p => p.PQRs)
                .HasForeignKey(d => d.IdPQRType)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PQR_PQRType");

            builder.HasOne(d => d.Customer)
                .WithMany(p => p.PQRs)
                .HasForeignKey(d => d.IdCustomer)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PQR_Customer");
        }
    }
}
