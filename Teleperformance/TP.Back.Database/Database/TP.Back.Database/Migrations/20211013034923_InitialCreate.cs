﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TP.Back.Database.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "TP");

            migrationBuilder.CreateTable(
                name: "Country",
                schema: "TP",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "VARCHAR(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DocumentType",
                schema: "TP",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "VARCHAR(5)", maxLength: 5, nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PQRType",
                schema: "TP",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "VARCHAR(5)", maxLength: 5, nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PQRType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "City",
                schema: "TP",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "VARCHAR(50)", maxLength: 50, nullable: false),
                    IdCountry = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_City", x => x.Id);
                    table.ForeignKey(
                        name: "FK_City_Country",
                        column: x => x.IdCountry,
                        principalSchema: "TP",
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                schema: "TP",
                columns: table => new
                {
                    Id = table.Column<long>(type: "BIGINT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "VARCHAR(100)", maxLength: 100, nullable: false),
                    IdDocumentType = table.Column<int>(type: "INT", nullable: false),
                    DocumentNumber = table.Column<string>(type: "VARCHAR(30)", maxLength: 30, nullable: false),
                    PhoneNumber = table.Column<string>(type: "VARCHAR(20)", maxLength: 20, nullable: true),
                    Birthday = table.Column<DateTime>(type: "DATE", nullable: false),
                    IdCity = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customer_City",
                        column: x => x.IdCity,
                        principalSchema: "TP",
                        principalTable: "City",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Customer_DocumentType",
                        column: x => x.IdDocumentType,
                        principalSchema: "TP",
                        principalTable: "DocumentType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PQR",
                schema: "TP",
                columns: table => new
                {
                    Id = table.Column<long>(type: "BIGINT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Identifier = table.Column<Guid>(type: "UNIQUEIDENTIFIER", nullable: false),
                    Comment = table.Column<string>(type: "NVARCHAR(800)", maxLength: 800, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    IdCustomer = table.Column<long>(type: "BIGINT", nullable: false),
                    IdPQRType = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PQR", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PQR_Customer",
                        column: x => x.IdCustomer,
                        principalSchema: "TP",
                        principalTable: "Customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PQR_PQRType",
                        column: x => x.IdPQRType,
                        principalSchema: "TP",
                        principalTable: "PQRType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_City_IdCountry",
                schema: "TP",
                table: "City",
                column: "IdCountry");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_IdCity",
                schema: "TP",
                table: "Customer",
                column: "IdCity");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_IdDocumentType",
                schema: "TP",
                table: "Customer",
                column: "IdDocumentType");

            migrationBuilder.CreateIndex(
                name: "IX_PQR_IdCustomer",
                schema: "TP",
                table: "PQR",
                column: "IdCustomer");

            migrationBuilder.CreateIndex(
                name: "IX_PQR_IdPQRType",
                schema: "TP",
                table: "PQR",
                column: "IdPQRType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PQR",
                schema: "TP");

            migrationBuilder.DropTable(
                name: "Customer",
                schema: "TP");

            migrationBuilder.DropTable(
                name: "PQRType",
                schema: "TP");

            migrationBuilder.DropTable(
                name: "City",
                schema: "TP");

            migrationBuilder.DropTable(
                name: "DocumentType",
                schema: "TP");

            migrationBuilder.DropTable(
                name: "Country",
                schema: "TP");
        }
    }
}
