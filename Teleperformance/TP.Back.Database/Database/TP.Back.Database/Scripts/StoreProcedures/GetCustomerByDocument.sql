USE [Teleperformance]
GO

-- =============================================
-- Author: Sebastian Rodriguez
-- Create date: 12/10/2021
-- Description:	Obtiene un cliente por el documento
-- =============================================
CREATE PROCEDURE [TP].[GetCustomerByDocument]
	-- Add the parameters for the stored procedure here
	@DocumentTypeCode AS VARCHAR(5),
	@DocumentNumber AS VARCHAR(30),
	@OUT_JSON AS VARCHAR(800) OUTPUT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT @OUT_JSON =
		(SELECT 
			C.Id,
			C.FullName,
			C.Birthday,
			C.DocumentNumber,
			DT.Code,
			C.PhoneNumber,
			C.IdCity,
			CT.Name
		FROM [TP].[Customer] C
			INNER JOIN [TP].[DocumentType] DT
		ON C.IdDocumentType = DT.Id
			INNER JOIN [TP].[City] CT
		ON CT.Id = C.IdCity
		WHERE 
			C.DocumentNumber = @DocumentNumber AND
			DT.Code = @DocumentTypeCode
		FOR JSON AUTO)
END
GO
