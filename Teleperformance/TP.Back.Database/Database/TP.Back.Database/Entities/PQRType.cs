﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TP.Back.Database.Entities
{
    [Table("PQRType", Schema = "TP")]
    public class PQRType
    {
        public PQRType()
        {
            PQRs = new HashSet<PQR>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public virtual ICollection<PQR> PQRs { get; set; }
    }
}
