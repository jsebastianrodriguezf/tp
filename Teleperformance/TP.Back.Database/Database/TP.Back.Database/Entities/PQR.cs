﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TP.Back.Database.Entities
{
    [Table("PQR", Schema = "TP")]
    public class PQR
    {
        public long Id { get; set; }
        public Guid Identifier { get; set; }
        public string Comment { get; set; }
        public DateTime CreationDate { get; set; }
        public long IdCustomer { get; set; }
        public int IdPQRType { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual PQRType PQRType { get; set; }
    }
}
