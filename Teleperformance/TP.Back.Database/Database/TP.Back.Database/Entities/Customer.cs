﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TP.Back.Database.Entities
{
    [Table("Customer", Schema = "TP")]
    public class Customer
    {
        public Customer()
        {
            PQRs = new HashSet<PQR>();
        }

        [Key]
        public long Id { get; set; }
        public string FullName { get; set; }
        public int IdDocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime Birthday { get; set; }
        public int IdCity { get; set; }

        public virtual DocumentType DocumentType { get; set; }
        public virtual City City { get; set; }

        public virtual ICollection<PQR> PQRs { get; set; }
    }
}
