﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TP.Back.Database.Entities
{
    [Table("Country", Schema = "TP")]
    public class Country
    {
        public Country()
        {
            Cities = new HashSet<City>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<City> Cities { get; set; }
    }
}
