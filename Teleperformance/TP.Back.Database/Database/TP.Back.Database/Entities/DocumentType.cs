﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TP.Back.Database.Entities
{
    [Table("DocumentType", Schema = "TP")]
    public class DocumentType
    {
        public DocumentType()
        {
            Customers = new HashSet<Customer>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
    }
}
