﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TP.Back.Database.Entities
{
    [Table("City", Schema = "TP")]
    public class City
    {
        public City()
        {
            Customers = new HashSet<Customer>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int IdCountry { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }

        public virtual Country Country { get; set; }
    }
}
