﻿using Microsoft.EntityFrameworkCore;
using System;
using TP.Back.Database.Configurations;
using TP.Back.Database.Entities;

namespace TP.Back.Database.Context
{
    public class TPContext : DbContext
    {
        public TPContext(DbContextOptions options) : base(options) { }

        #region DBSet
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<DocumentType> DocumentTypes { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<PQR> PQRs { get; set; }
        public virtual DbSet<PQRType> PQRTypes { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            //modelBuilder.ApplyConfigurationsFromAssembly(typeof(TPContext).Assembly);

            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new DocumentTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CityConfiguration());
            modelBuilder.ApplyConfiguration(new CountryConfiguration());
            modelBuilder.ApplyConfiguration(new PQRConfiguration());
            modelBuilder.ApplyConfiguration(new PQRTypeConfiguration());
        }
    }
}
