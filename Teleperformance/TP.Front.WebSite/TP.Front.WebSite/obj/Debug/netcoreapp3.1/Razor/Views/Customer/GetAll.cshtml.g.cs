#pragma checksum "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d611fc8e8134259b5db88717364154a5739c6160"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Customer_GetAll), @"mvc.1.0.view", @"/Views/Customer/GetAll.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\_ViewImports.cshtml"
using TP.Front.WebSite;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\_ViewImports.cshtml"
using TP.Front.WebSite.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
using TP.Front.Application.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
using TP.Transverse.Models.Response.Customer;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d611fc8e8134259b5db88717364154a5739c6160", @"/Views/Customer/GetAll.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9975f37e664412443125faa09e534073f346168c", @"/Views/_ViewImports.cshtml")]
    public class Views_Customer_GetAll : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CustomerByFilterModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/home.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
  
    ViewData["Title"] = "Clientes";
    

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "d611fc8e8134259b5db88717364154a5739c61604590", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n");
#nullable restore
#line 10 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
 using (Html.BeginForm("GetAll", "Customer"))
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <table>\r\n        <tr>\r\n            <td>\r\n                <b>Numero de Documento</b><br />\r\n                ");
#nullable restore
#line 16 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
           Write(Html.TextBoxFor(x => x.DocumentNumber));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                <b>Ciudad</b><br />\r\n                ");
#nullable restore
#line 20 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
           Write(Html.DropDownListFor(x => x.IdCity, (IEnumerable<SelectListItem>)ViewBag.optionsCities, "Seleccione"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                <b>Desde</b><br />\r\n                ");
#nullable restore
#line 24 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
           Write(Html.TextBoxFor(x => x.From, new { type = "date" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                <b>Hasta</b><br />\r\n                ");
#nullable restore
#line 28 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
           Write(Html.TextBoxFor(x => x.Until, new { type = "date" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                &nbsp; &nbsp;\r\n                <button type=\"submit\">Buscar</button>\r\n            </td>\r\n        </tr>\r\n    </table>\r\n");
#nullable restore
#line 34 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("<br />\r\n<br />\r\n\r\n<div");
            BeginWriteAttribute("class", " class=\"", 1062, "\"", 1070, 0);
            EndWriteAttribute();
            WriteLiteral(@">
    <table>
        <thead>
            <tr class=""tr-header-table"">
                <td><b>Nombre</b></td>
                <td><b>Tipo Documento</b></td>
                <td><b>No. Documento</b></td>
                <td><b>F. Nacimiento</b></td>
                <td><b>No. Celular</b></td>
                <td><b>Ciudad</b></td>
            </tr>
        </thead>
        <tbody>
");
#nullable restore
#line 51 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
             foreach (var customer in (List<CustomerResponse>)ViewBag.customerModel.CustomerssModel)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <tr>\r\n                    <td><a");
            BeginWriteAttribute("href", " href=\"", 1632, "\"", 1689, 2);
            WriteAttributeValue("", 1639, "../Customer/Update?idCustomer=", 1639, 30, true);
#nullable restore
#line 54 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
WriteAttributeValue("", 1669, customer.IdCustomer, 1669, 20, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 54 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
                                                                                Write(customer.FullName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a></td>\r\n                    <td>");
#nullable restore
#line 55 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
                   Write(customer.DocumentTypeCode);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                    <td>");
#nullable restore
#line 56 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
                   Write(customer.DocumentNumber);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                    <td>");
#nullable restore
#line 57 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
                   Write(customer.Birthday.ToString("dd/MM/yyyy"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                    <td>");
#nullable restore
#line 58 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
                   Write(customer.PhoneNumber);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                    <td>");
#nullable restore
#line 59 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
                   Write(customer.CityName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                </tr>\r\n");
#nullable restore
#line 61 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </tbody>\r\n    </table>\r\n</div>\r\n<br />\r\n\r\n<b>Edad promedio de Clientes:</b>\r\n");
#nullable restore
#line 68 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
Write(ViewBag.customerModel.CustomersAverage?.OldAverage);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    &nbsp; &nbsp;\r\n<b>Personas menores de 20:</b>\r\n");
#nullable restore
#line 71 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
Write(ViewBag.customerModel.CustomersAverage?.NumberYoungCustomers);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    &nbsp; &nbsp;\r\n<b>Personas mayores de 50:</b>\r\n");
#nullable restore
#line 74 "I:\Teleperformance\WebApp\Teleperformance\TP.Front.WebSite\TP.Front.WebSite\Views\Customer\GetAll.cshtml"
Write(ViewBag.customerModel.CustomersAverage?.NumberOldCustomers);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CustomerByFilterModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
