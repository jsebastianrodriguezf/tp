﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using System.Security.Authentication;
using TP.Front.Application.Utility.Constants;
using TP.Front.Application.Utility.ExternalServices;

namespace TP.Front.WebSite.Installer
{
    public class HttpClientInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpClient(HttpClientConstants.TP_Back_API_Client, client =>
            {
                client.BaseAddress = new Uri(configuration.GetSection("TPBackAPI:BaseRoute").Value);
            }).
            ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler()
            {
                SslProtocols = SslProtocols.Tls12 | SslProtocols.Tls11 | SslProtocols.Tls,
                ClientCertificateOptions = ClientCertificateOption.Automatic,
                PreAuthenticate = true
            });

            services.AddTransient<IConsumeExternalService, ConsumeExternalService>();
        }
    }
}
