﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TP.Front.Application.Repository.Implementations;
using TP.Front.Application.Repository.Interfaces;
using TP.Front.Application.Services.Implementations;
using TP.Front.Application.Services.Interfaces;
using TP.Front.Application.Utility.AppSettingsKeys;
using TP.Front.WebSite.Utility;

namespace TP.Front.WebSite.Installer
{
    public class ApplicationInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<TPFrontAPIKeys>(configuration.GetSection("TPBackAPI"));

            services.AddScoped<DocumentTypeUtility>();
            services.AddScoped<CityUtility>();
            services.AddScoped<PQRTypeUtility>();

            services.AddScoped<ICityService, CityService>();
            services.AddScoped<IPQRService, PQRService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IDocumentTypeService, DocumentTypeService>();

            services.AddTransient<IPQRRepository, PQRRepository>();
            services.AddTransient<ICustomerRepository, CustomerRepository>();
            services.AddTransient<IDocumentTypeRepository, DocumentTypeRepository>();
            services.AddTransient<ICityRepository, CityRepository>();
        }
    }
}
