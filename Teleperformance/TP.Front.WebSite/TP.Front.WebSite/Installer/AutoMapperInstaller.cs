﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TP.Front.Application.Utility.Profiles;

namespace TP.Front.WebSite.Installer
{
    public class AutoMapperInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper(typeof(CustomerProfile));
            //services.AddAutoMapper(typeof(CityProfile));
            services.AddAutoMapper(typeof(DocumentTypeProfile));
            //services.AddAutoMapper(typeof(PQRProfile));
        }
    }
}
