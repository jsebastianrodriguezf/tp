﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Threading.Tasks;
using TP.Front.Application.Services.Interfaces;
using TP.Transverse.Models.Response.PQR;

namespace TP.Front.WebSite.Utility
{
    public class PQRTypeUtility
    {
        private readonly IPQRService pQRService;

        public PQRTypeUtility(IPQRService pQRService)
        {
            this.pQRService = pQRService;
        }

        public async Task<List<SelectListItem>> PQRTypes(string selectedValue = null)
        {
            List<SelectListItem> itemsList = new List<SelectListItem>();
            List<PQRTypeResponse> pQRsTypes = await pQRService.GetAllPQRTypes();
            pQRsTypes.ForEach(x => itemsList.Add(
                new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Code,
                    Selected = x.Code == selectedValue
                }));

            return itemsList;
        }
    }
}
