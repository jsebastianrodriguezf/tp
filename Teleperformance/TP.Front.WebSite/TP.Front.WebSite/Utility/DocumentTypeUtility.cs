﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Threading.Tasks;
using TP.Front.Application.Models;
using TP.Front.Application.Services.Interfaces;

namespace TP.Front.WebSite.Utility
{
    public class DocumentTypeUtility
    {
        private readonly IDocumentTypeService documentTypeService;

        public DocumentTypeUtility(IDocumentTypeService documentTypeService)
        {
            this.documentTypeService = documentTypeService;
        }

        public async Task<List<SelectListItem>> DocumentTypes()
        {
            List<SelectListItem> itemsList = new List<SelectListItem>();
            List<DocumentTypeModel> documentTypes = await documentTypeService.GetAllDocumentTypes();
            documentTypes.ForEach(x => itemsList.Add(new SelectListItem() { Text = x.Name, Value = x.Code }));

            return itemsList;
        }
    }
}