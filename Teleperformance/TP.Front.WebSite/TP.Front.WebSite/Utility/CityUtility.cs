﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Threading.Tasks;
using TP.Front.Application.Services.Interfaces;
using TP.Transverse.Models.Response.City;

namespace TP.Front.WebSite.Utility
{
    public class CityUtility
    {
        private readonly ICityService cityService;

        public CityUtility(ICityService cityService)
        {
            this.cityService = cityService;
        }

        public async Task<List<SelectListItem>> Cities(string selectedValue = null)
        {
            List<SelectListItem> itemsList = new List<SelectListItem>();
            List<CityResponse> cities = await cityService.GetAllCities();
            cities.ForEach(x => itemsList.Add(
                new SelectListItem() { 
                    Text = x.Name, 
                    Value = x.Id,
                    Selected = x.Id == selectedValue
                }));

            return itemsList;
        }
    }
}
