﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TP.Front.Application.Models;
using TP.Front.Application.Services.Interfaces;
using TP.Front.WebSite.Utility;
using TP.Transverse.Models.Response.Customer;

namespace TP.Front.WebSite.Controllers
{
    public class TeleperformanceController : Controller
    {
        private readonly DocumentTypeUtility documentTypeUtility;
        private readonly ICustomerService customerService;

        public TeleperformanceController(DocumentTypeUtility documentTypeUtility, ICustomerService customerService)
        {
            this.documentTypeUtility = documentTypeUtility;
            this.customerService = customerService;
        }

        public async Task<IActionResult> Index()
        {
            List<SelectListItem> itemsList = await documentTypeUtility.DocumentTypes();

            ViewBag.optionsDocumentTypes = itemsList;

            return View();
        }

        public async Task<RedirectToActionResult> SearchCustomer(CustomerByDocumentModel request)
        {
            if (!ModelState.IsValid) 
                return RedirectToAction("Index");

            CustomerResponse customer = await customerService.GetCustomerByDocument(request);

            if (customer?.IdCustomer == null)
                return RedirectToAction("Create", "Customer");
            else
                return RedirectToAction("Update", "Customer", new { idCustomer = customer.IdCustomer });
        }

    }
}
