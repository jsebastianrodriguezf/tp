﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TP.Front.Application.Models;
using TP.Front.Application.Services.Interfaces;
using TP.Front.WebSite.Utility;
using TP.Transverse.Models.Response.PQR;

namespace TP.Front.WebSite.Controllers
{
    public class PQRController : Controller
    {
        private readonly IPQRService pQRService;
        private readonly PQRTypeUtility pQRTypeUtility;
        private readonly CityUtility cityUtility;
        public PQRController(IPQRService pQRService, PQRTypeUtility pQRTypeUtility, CityUtility cityUtility)
        {
            this.pQRService = pQRService;
            this.pQRTypeUtility = pQRTypeUtility;
            this.cityUtility = cityUtility;
        }

        public async Task<IActionResult> Create(long idCustomer)
        {
            ViewBag.optionsPQRTypes = await pQRTypeUtility.PQRTypes();

            CreatePQRModel createPQRModel = new CreatePQRModel()
            {
                IdCustomer = idCustomer
            };

            return View(createPQRModel);
        }

        public async Task<IActionResult> GetAll(PQRByFilterModel request)
        {
            GetAllPQRByFilterModel pQRFilter = await pQRService.GetPQRByFilter(request);

            ViewBag.optionsCities = await cityUtility.Cities(request?.IdCity.ToString());
            ViewBag.pqrmodel = pQRFilter;

            return View(request);
        }

        public async Task<IActionResult> Get(string id)
        {
            PQRModel pQRModel = null;
            if (Guid.TryParse(id, out Guid identifier))
                pQRModel = await pQRService.GetPQRByIdentifier(identifier);

            if(pQRModel == null)
                return RedirectToAction("GetAll", "PQR");

            return View(pQRModel);
        }

        public async Task<IActionResult> CreatePQR(CreatePQRModel request)
        {
            if (!ModelState.IsValid) 
                return RedirectToAction("Create", "PQR");

            long customerId = request.IdCustomer;
            PQRResponse pQRResponse = await pQRService.CreatePQR(request);

            if(pQRResponse == null)
                return RedirectToAction("Create", "PQR", new { idCustomer = customerId });

            return RedirectToAction("GetAll", "PQR");
        }
    }
}
