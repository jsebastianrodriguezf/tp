﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TP.Front.Application.Models;
using TP.Front.Application.Services.Interfaces;
using TP.Front.WebSite.Utility;
using TP.Transverse.Models.Response.Customer;

namespace TP.Front.WebSite.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerService customerService;
        private readonly DocumentTypeUtility documentTypeUtility;
        private readonly CityUtility cityUtility;

        public CustomerController(ICustomerService customerService, CityUtility cityUtility, DocumentTypeUtility documentTypeUtility)
        {
            this.cityUtility = cityUtility;
            this.documentTypeUtility = documentTypeUtility;
            this.customerService = customerService;
        }

        public async Task<IActionResult> Create()
        {
            ViewBag.optionsDocumentTypes = await documentTypeUtility.DocumentTypes();
            ViewBag.optionsCities = await cityUtility.Cities();
            return View();
        }

        public async Task<IActionResult> GetAll(CustomerByFilterModel request)
        {
            GetAllCustomerByFilterModel customerFilter = await customerService.GetCustomerByFilter(request);

            ViewBag.optionsCities = await cityUtility.Cities(request?.IdCity.ToString());
            ViewBag.customerModel = customerFilter;

            return View(request);
        }

        public async Task<IActionResult> Update(long idCustomer)
        {
            CustomerResponse customerResponse = await customerService.GetCustomer(idCustomer);
            if (customerResponse?.IdCustomer == null )
                return RedirectToAction("Index", "Teleperformance");

            ViewBag.optionsCities = await cityUtility.Cities(customerResponse?.IdCity.ToString());
            ViewBag.Customer = customerResponse;

            UpdateCustomerModel updateCustomerModel = new UpdateCustomerModel
            {
                PhoneNumber = customerResponse.PhoneNumber,
                IdCustomer = customerResponse.IdCustomer,
                IdCity = customerResponse.IdCity
            };
            return View(updateCustomerModel);
        }

        public async Task<IActionResult> CreateCustomer(CreateCustomerModel request)
        {
            if (ModelState.IsValid)
            {
                request.PhoneNumber ??= string.Empty;

                CustomerResponse customerResponse = await customerService.CreateCustomer(request);

                if (customerResponse != null)
                    return RedirectToAction("Update", "Customer", new { idCustomer = customerResponse.IdCustomer });
            }
            return RedirectToAction("Create");
        }

        public async Task<IActionResult> UpdateCustomer(UpdateCustomerModel request)
        {
            if (ModelState.IsValid)
            {
                CustomerResponse customerResponse = await customerService.UpdateCustomer(request);

                if (customerResponse?.IdCustomer != null)
                    return RedirectToAction("Update", "Customer", new { idCustomer = customerResponse.IdCustomer });
            }

            return RedirectToAction("Create", "Customer");
        }
    }
}
